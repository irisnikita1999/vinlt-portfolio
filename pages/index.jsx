// Libraries
import ReactFullPage from "@fullpage/react-fullpage";
import dynamic from "next/dynamic";
import { getPostList } from "services/post";

// Utils
import { addClassBySelector, isMobile } from "utils";

// Components
const Layout = dynamic(() => import("components/Layout"));
const AudioPlayer = dynamic(() => import("components/AudioPlayer"));

// Modules
const Blog = dynamic(() => import("module/Home/Blog"));
const AboutSection = dynamic(() => import("module/Home/AboutSection"));
const IntroduceSection = dynamic(() => import("module/Home/IntroduceSection"));
const ExperiencesSection = dynamic(() =>
  import("module/Home/ExperiencesSection")
);
const SkillsSection = dynamic(() => import("module/Home/SkillsSection"));
const PortfolioSection = dynamic(() => import("module/Home/PortfolioSection"));
const ContactSection = dynamic(() => import("module/Home/ContactSection"));

const defaultAnchors = [
  "home",
  "about",
  "skills",
  "experiences",
  "portfolio",
  "blog",
  "contact",
];

export async function getStaticProps(context) {
  const posts = await getPostList({
    _sort: "createdAt:DESC",
    _limit: 3,
  });

  return {
    props: {
      posts,
    },
    revalidate: 30, // will be passed to the page component as props
  };
}

export default function Home({ posts }) {
  const onLeaveFullPage = (_, des) => {
    Array.from(document.querySelectorAll(".menu-item")).forEach((el) => {
      el.classList.remove("active");
    });

    if (!isMobile.any()) {
      const desItem = document.querySelector(`[data-menuanchor=${des.anchor}]`);
      const lineBottom = document.getElementById("line-bottom");

      desItem.classList.add("active");
      lineBottom.style.transform = `translate(${desItem.offsetLeft}px,0px)`;
      lineBottom.style.width = `${desItem.offsetWidth}px`;

      switch (des.index) {
        case 1:
          addClassBySelector("#lottie-develop", "animate__fadeInLeft");
          addClassBySelector("#about-me-title", "animate__fadeInUp");
          addClassBySelector("#about-me-description", "animate__fadeInUp");
          addClassBySelector("#about-me-description-2", "animate__fadeInUp");
          addClassBySelector("#good-at", "animate__fadeInUp");
          addClassBySelector("#cv-btn", "animate__fadeIn");
          break;
        case 2:
          addClassBySelector(".skill-item", "animate__fadeInUp");
          break;

        case 3:
          addClassBySelector(".experience-card", "animate__fadeInUp");
          addClassBySelector("#education", "animate__fadeInUp");
          addClassBySelector("#work-experience", "animate__fadeIn");
          break;

        case 4:
          addClassBySelector("#moneytech", "animate__fadeInDown");
          addClassBySelector("#habimecgroup", "animate__fadeInUp");
          addClassBySelector("#social-chat", "animate__fadeInDown");
          addClassBySelector("#error-monitoring", "animate__fadeInUp");
          addClassBySelector("#propzy", "animate__fadeInUp");
          addClassBySelector("#recent-portfolio-title", "animate__fadeInUp");
          break;

        case 5:
          addClassBySelector("#featured-title", "animate__fadeInUp");
          addClassBySelector(".post-card", "animate__fadeInUp");
          break;

        case 6:
          addClassBySelector(".via-contact", "animate__fadeInUp");
          addClassBySelector("#contact-title", "animate__fadeInLeft");
          addClassBySelector("#contact-description", "animate__fadeInLeft");
          addClassBySelector(".contact-input", "animate__fadeInUp");
          addClassBySelector(".submit-btn", "animate__fadeInUp");
          break;

        default:
          break;
      }

      return;
    } else {
      Array.from(document.querySelectorAll(".header-menu-item")).forEach(
        (el) => {
          el.classList.remove("active");
        }
      );

      const desItem = document.querySelector(
        `[data-header-menu-anchor=${des.anchor}]`
      );

      desItem.classList.add("active");
    }
  };

  const afterRender = () => {
    const firstItemHeader = document.getElementsByClassName("menu-item")[0];
    const firstHeaderMenuItem =
      document.getElementsByClassName("header-menu-item")[0];
    const lineBottom = document.getElementById("line-bottom");

    firstItemHeader.classList.add("active");
    firstHeaderMenuItem.classList.add("active");

    lineBottom.style.width = `${firstItemHeader.offsetWidth}px`;
  };

  return (
    <Layout>
      <div className="container mx-auto text-gray-800 xl:mt-0 mt-24">
        <div className="absolute top-28 left-4 z-50 opacity-0 md:opacity-100">
          <AudioPlayer />
        </div>
        <ReactFullPage
          scrollOverflow={true}
          responsiveWidth={1200}
          scrollingSpeed={1000}
          navigation
          afterRender={afterRender}
          onLeave={onLeaveFullPage}
          anchors={defaultAnchors}
          render={() => (
            <ReactFullPage.Wrapper>
              <section className="section">
                <IntroduceSection />
              </section>
              <section className="section">
                <AboutSection />
              </section>
              <section className="section">
                <SkillsSection />
              </section>
              <section className="section">
                <ExperiencesSection />
              </section>
              <section className="section">
                <PortfolioSection />
              </section>
              <section className="section">
                <Blog posts={posts} />
              </section>
              <section className="section">
                <ContactSection />
              </section>
            </ReactFullPage.Wrapper>
          )}
        />
      </div>
    </Layout>
  );
}
