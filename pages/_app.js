import "../styles/globals.scss";
import "tailwindcss/tailwind.css";
import "animate.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
