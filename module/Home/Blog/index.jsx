// Libraries
import React from "react";
import moment from "moment";

const STRAPI_URL = process.env.NEXT_PUBLIC_STRAPI_URL;

const Blog = ({ posts }) => {
  return (
    <div className="text-center mt-10 xl:mt-0">
      <h2
        id="featured-title"
        className="animate__animated text-3xl font-semibold"
      >
        Featured Posts
      </h2>
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-10 mt-10">
        {Array.isArray(posts)
          ? posts.map((post, index) => {
              const {
                id,
                image,
                categories = [],
                title,
                description,
                slug,
                createdAt,
              } = post;
              return (
                <div
                  key={id}
                  className={`animate__animated post-card animate__slow rounded-lg shadow-custom overflow-hidden flex flex-col ani__delay-card--${index}`}
                >
                  <div
                    className="w-full h-56"
                    style={{
                      backgroundImage: `url(${STRAPI_URL}${image.url})`,
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                    }}
                  ></div>
                  <div className="flex flex-col text-left p-4 justify-between flex-1">
                    <div>
                      <div className="flex flex-wrap gap-2 mb-2">
                        {Array.isArray(categories) &&
                          categories.map((category) => {
                            return (
                              <div
                                key={category.id}
                                className="py-1 px-2 text-xs border transition-all cursor-pointer hover:text-white hover:bg-cyan-700 text-cyan-800 border-cyan-700 rounded-sm"
                              >
                                {category.name}
                              </div>
                            );
                          })}
                      </div>
                      <h2 className="mb-2 text-xl text-gray-900 font-semibold">
                        {title}
                      </h2>
                      <div className="mb-2 text-cyan-800 text-sm">
                        {moment(createdAt).format("LL")}
                      </div>
                      <div className="mb-2 truncate-3">{description}</div>
                    </div>
                    <a
                      className="outline-none flex items-center border-none font-semibold text-cyan-700 hover:text-cyan-600 w-max"
                      target="_blank"
                      href={`${process.env.NEXT_PUBLIC_BLOG_API}/posts/${slug}`}
                      rel="noreferrer"
                    >
                      Read more
                    </a>
                  </div>
                </div>
              );
            })
          : null}
      </div>
    </div>
  );
};

Blog.propTypes = {};

export default Blog;
