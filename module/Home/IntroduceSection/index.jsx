// Libraries
import React from "react";
import Image from "next/image";

function IntroduceSection() {
  return (
    <div className="grid lg:grid-cols-2 grid-cols-1 gap-5">
      <div className="lg:w-max flex md:row-start-1 row-start-2 lg:items-start items-center flex-col justify-center text-center lg:text-left">
        <div className="animate__animated animate__slow intro-child bg-cyan-700 w-max rounded-r-full py-2 px-7 text-white font-medium rounded-tl-full md:rounded-bl-none rounded-bl-full">
          Hello I&apos;m
        </div>
        <h1 className="intro-child text-6xl font-bold my-4 animate__animated animate__slow">
          Vi Nguyen
        </h1>
        <h2 className="intro-child text-4xl font-medium animate__animated animate__slow animate__delay-half">
          Front-end developer
        </h2>
        <div className="mt-5 lg:text-left text-center text-gray-900">
          <div className="intro-child mb-2 transition-all hover:text-cyan-700 animate__animated animate__slow animate__delay-1s">
            <i className="icon-portfolio-mail mr-2"></i>
            <a href="mailto:nltruongvi@gmail.com">nltruongvi@gmail.com</a>
          </div>
          <div className="intro-child mb-2 transition-all hover:text-cyan-700 animate__animated animate__slow animate__delay-1s">
            <i className="icon-portfolio-phone mr-2"></i>
            <a href="tel:+84797056915">+84 797 056 915</a>
          </div>
          <div className="intro-child transition-all hover:text-cyan-700 animate__animated animate__slow animate__delay-1s">
            <i className="icon-portfolio-location mr-2"></i>
            Binh Thanh District, Ho Chi Minh City
          </div>
        </div>
        <div className="intro-child mt-5 flex items-center gap-3 animate__animated animate__slow animate__delay-1s">
          <a
            rel="noreferrer"
            href="https://www.facebook.com/profile.php?id=100010260734048"
            target="_blank"
            aria-label="facebook"
            className="flex items-center justify-center h-10 w-10 rounded-md hover:bg-cyan-700 hover:text-white transition-all"
          >
            <i className="icon-portfolio-facebook"></i>
          </a>
          <a
            rel="noreferrer"
            target="_blank"
            href="https://github.com/irisnikita"
            aria-label="github"
            className="flex items-center justify-center h-10 w-10 rounded-md hover:bg-cyan-700 hover:text-white transition-all"
          >
            <i className="icon-portfolio-github"></i>
          </a>
          <a
            rel="noreferrer"
            target="_blank"
            href="https://www.linkedin.com/in/nguyen-vi-b52052218/"
            aria-label="linkedin"
            className="flex items-center justify-center h-10 w-10 rounded-md hover:bg-cyan-700 hover:text-white transition-all"
          >
            <i className="icon-portfolio-linkedin"></i>
          </a>
        </div>
      </div>
      <div>
        <div
          id="intro-avatar"
          className="rounded-full lg:float-right mx-auto lg:h-96 lg:w-96 h-64 w-64 border-8 border-gray-300 relative overflow-hidden animate__animated animate__slow"
        >
          <Image
            src="/images/profile/IMG_0494.jpg"
            layout="fill"
            loading="eager"
            alt="Avatar image"
          ></Image>
        </div>
      </div>
    </div>
  );
}

export default IntroduceSection;
