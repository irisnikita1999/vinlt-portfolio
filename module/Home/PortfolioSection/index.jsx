// Libraries
import React, { useState } from "react";
import Image from "next/image";

// Styles
import styles from "./styles.module.scss";
import PortfolioCard from "components/PortfolioCard";

function PortfolioSection() {
  const [cols] = useState([
    {
      value: "col-1",
      projects: [
        {
          value: "moneytech",
          label: "Moneytech",
          image: "/images/projects/moneytech.png",
          height: "h-72",
          className: "origin-top-left",
          images: [
            {
              description: "Home page",
              src: "/images/projects/moneytech/home-page.png",
            },
            {
              description: "About us page",
              src: "/images/projects/moneytech/about-us-page.png",
            },
            {
              description: "Contact us page",
              src: "/images/projects/moneytech/contact-us-page.png",
            },
            {
              description: "Finance page",
              src: "/images/projects/moneytech/finance-page.png",
            },
            {
              description: "News page",
              src: "/images/projects/moneytech/news-page.png",
            },
            {
              description: "Pinnacle partners page",
              src: "/images/projects/moneytech/pinnacle-partners-page.png",
            },
          ],
          title: "MoneyTech",
          description: (
            <>
              We are Australia&apos;s only non-bank financial institution that
              offers finance, payments and FX capabilities. <br /> This gives us
              a unique position to help SMEs who are the growth engine of
              Australia and employ the majority of Australians. <br /> Our
              working capital solutions, market leading FX and an enterprise
              level payments platform gives you control over YOUR cashflow and
              payments, saving you time and money so you can concentrate on
              doing what you love!
            </>
          ),
          link: "https://www.moneytech.com.au/",
          skills: ["Html", "Css", "Javascript", "Webflow"],
        },
        {
          value: "habimecgroup",
          label: "Habimecgroup",
          image: "/images/projects/habimec.jpg",
          height: "h-52",
          className: "origin-bottom-left",
          images: [
            {
              description: "Home page",
              src: "/images/projects/habimec/home-page.png",
            },
            {
              description: "About us page",
              src: "/images/projects/habimec/about-us-page.png",
            },
            {
              description: "Contact page",
              src: "/images/projects/habimec/contact-page.png",
            },
            {
              description: "Product page",
              src: "/images/projects/habimec/product-page.png",
            },
            {
              description: "Certificate page",
              src: "/images/projects/habimec/certificate-page.png",
            },
          ],
          title: "Habimecgroup",
          description: (
            <>
              We are a leading manufacturer in medical equipment with extensive
              experience in manufacturing and exporting to Europe and America.
              The outbreak of Corona virus has caused a serious global health
              crisis, being able to protect public health and prevent the spread
              of Covid-19 is one of our main goals and we would like to
              introduce some of our main products that include: Face masks,
              anti-epidemic clothing, gloves and other protective equipment that
              can help to prevent the spread of Corona virus.
            </>
          ),
          link: "https://habimecgroup.com.vn/en/home/",
          skills: ["Html", "Css", "Javascript", "ReactJs", "NextJs"],
        },
      ],
    },
    {
      value: "col-2",
      projects: [
        {
          value: "social-chat",
          label: "Social chat",
          image: "/images/projects/social-chat.png",
          height: "h-52",
          className: "origin-top left-0",
          cardDes: "(This content not yet Updated)",
        },
        {
          value: "error-monitoring",
          label: "Error monitoring",
          image: "/images/projects/error-monitoring.jpg",
          height: "h-72",
          className: "origin-bottom left-0",
          cardDes: "(This content not yet updated)",
        },
      ],
    },
    {
      value: "col-3",
      projects: [
        {
          value: "propzy",
          label: "Propzy Tet",
          image: "/images/projects/propzy.png",
          height: "h-106",
          className: "origin-right right-0",
          cardDes: "(This content not yet updated)",
        },
      ],
    },
  ]);

  return (
    <div className="text-center mt-10 xl:mt-0">
      <h2
        id="recent-portfolio-title"
        className="animate__animated text-3xl font-semibold"
      >
        Recent Portfolio
      </h2>
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-6 pt-5 pb-2 mt-5 relative">
        {cols.map((col) => (
          <div key={col.value} className="flex flex-col gap-6">
            {col.projects.map((project) => (
              <PortfolioCard
                key={project.value}
                className={project.className}
                images={project.images}
                title={project.title}
                description={project.description}
                link={project.link}
                skills={project.skills}
              >
                <div
                  id={project.value}
                  className={`relative group cursor-pointer ${styles["project-card"]} ${project.height} rounded-xl animate__animated animate__slow overflow-hidden shadow-custom`}
                >
                  <div className="w-full flex flex-col items-center justify-center rounded-xl text-white absolute h-full transition-all duration-500 z-10 bg-cyan-700 group-hover:bg-opacity-90 group-hover:opacity-100 opacity-0 bg-opacity-0">
                    <i className="icon-portfolio-search text-2xl"></i>
                    <div
                      className={`font-semibold text-xl ${styles["__label"]}`}
                    >
                      {project.label}
                    </div>
                    <div className={`text-sm opacity-80 ${styles["__des"]}`}>
                      {project.cardDes}
                    </div>
                  </div>
                  <Image
                    layout="fill"
                    loading="eager"
                    objectFit="cover"
                    objectPosition="top"
                    src={project.image}
                    alt={project.label}
                  />
                </div>
              </PortfolioCard>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default PortfolioSection;
