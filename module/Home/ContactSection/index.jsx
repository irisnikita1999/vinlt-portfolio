// Libraries
import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import Lottie from "react-lottie";
import * as Yup from "yup";

// Lottie animation
import sendMailAnimation from "public/Lottie/send-email.json";
import { sendEmail } from "services/contact";

// Schema
const schema = Yup.object().shape({
  fullName: Yup.string().trim().required("Full name is required"),
  email: Yup.string()
    .trim()
    .email("Invalid email")
    .required("Email is required"),
  message: Yup.string().trim(),
});

// Options Schema
const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: sendMailAnimation,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const ContactSection = () => {
  const [isSubmitting, setSubmitting] = useState(false);

  return (
    <div className="mt-10 xl:mt-0">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-10 mt-5">
        <div className="text-left md:text-right">
          <h2
            id="contact-title"
            className="animate__animated animate__slow text-3xl font-semibold"
          >
            Contact Me
          </h2>
          <p
            id="contact-description"
            className="mt-5 animate__animated animate__slow"
          >
            Feel free to get in touch with me, I am always open to discussing
            new projects, creative ideas or opportunities to be part of your
            visions.
          </p>
          <div className="mt-5 text-gray-900">
            <div className="flex items-center justify-end md:flex-row flex-row-reverse gap-2 via-contact mb-2 transition-all hover:text-cyan-700 animate__animated animate__slow ani__delay-1">
              <a href="mailto:nltruongvi@gmail.com">nltruongvi@gmail.com</a>
              <i className="icon-portfolio-mail"></i>
            </div>
            <div className="flex items-center justify-end md:flex-row flex-row-reverse gap-2 via-contact mb-2 transition-all hover:text-cyan-700 animate__animated animate__slow ani__delay-1-5">
              <a href="tel:+84797056915">+84 797 056 915</a>
              <i className="icon-portfolio-phone"></i>
            </div>
            <div className="flex items-center justify-end md:flex-row flex-row-reverse gap-2 via-contact transition-all hover:text-cyan-700 animate__animated animate__slow ani__delay-2">
              Binh Thanh District, Ho Chi Minh City
              <i className="icon-portfolio-location"></i>
            </div>
          </div>
        </div>

        <div>
          <Formik
            initialValues={{ fullName: "", email: "", message: "" }}
            validationSchema={schema}
            onSubmit={async (values) => {
              setSubmitting(true);

              await sendEmail(values);
            }}
          >
            {() => {
              return isSubmitting ? (
                <div className="animate__animated animate__fadeIn flex flex-col justify-center items-center">
                  <Lottie options={defaultOptions} width={200}></Lottie>
                  <p className="font-semibold">
                    Thanks for get in touch with me, I will contact you soon
                  </p>
                </div>
              ) : (
                <Form className="flex flex-col gap-5">
                  <div>
                    <Field
                      type="text"
                      name="fullName"
                      placeholder="Enter your name"
                      className="contact-input animate__animated p-3 outline-none border rounded-md border-gray-700 w-full"
                    />
                    <ErrorMessage
                      name="fullName"
                      component="div"
                      className="text-red-800 mt-1 animate__animated animate__fadeIn"
                    />
                  </div>
                  <div>
                    <Field
                      type="email"
                      name="email"
                      placeholder="Enter your email"
                      className="contact-input animate__animated ani__delay-half p-3 outline-none border rounded-md border-gray-700 w-full"
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="text-red-800 mt-1 animate__animated animate__fadeIn"
                    />
                  </div>
                  <div>
                    <Field
                      type="text"
                      as="textarea"
                      rows="5"
                      name="message"
                      placeholder="Enter your message"
                      className="contact-input animate__animated ani__delay-1 p-3 outline-none border rounded-md border-gray-700 resize-none w-full"
                    />
                    <ErrorMessage
                      name="message"
                      component="div"
                      className="text-red-800 mt-1"
                    />
                  </div>
                  <button
                    type="submit"
                    className={`submit-btn mb-10 xl:mb-0 animate__animated ${
                      isSubmitting ? "opacity-70 pointer-events-none" : ""
                    } ani__delay-1-5 bg-cyan-700 w-full rounded-md py-3 px-7 text-white font-medium`}
                  >
                    Submit
                  </button>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};

ContactSection.propTypes = {};

export default ContactSection;
