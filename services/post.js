import axios from "services";

const endpoint = process.env.NEXT_PUBLIC_STRAPI_URL + "/articles";

export const getPostList = async (params) => {
  try {
    const { data } = await axios.get(endpoint, {
      params,
    });

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getPost = async (id) => {
  try {
    const { data } = await axios.get(`${endpoint}/${id}`);

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};
