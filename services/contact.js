import axios from "services";

const endpoint = process.env.NEXT_PUBLIC_API_URL + "contact";

export const sendEmail = async (form) => {
  try {
    const { data } = await axios.post(endpoint, form);

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};
