// Libraries
import React, { useEffect, useMemo, useState } from "react";
import { Howl } from "howler";
import Lottie from "react-lottie";

// Lottie
import waveAnimation from "public/Lottie/wave.json";

// Styles
import styles from "./styles.module.scss";

const AudioPlayer = () => {
  const [percent, setPercent] = useState(0);
  const [isPlaying, setPlaying] = useState(false);
  const [isSlide, setSlide] = useState(false);

  const defaultOptions = {
    autoplay: false,
    animationData: waveAnimation,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const sound = useMemo(() => {
    return new Howl({
      src: [
        "https://s3.us-east-2.amazonaws.com/vi.nlt-m-bucket/take-me-hand.mp3",
      ],
      format: ["mp3", "aac"],
      volume: 0.5,
      html5: true,
      onplay(value) {
        setPlaying(true);
      },
      onend() {
        setPlaying(false);
      },
    });
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      if (sound.seek() !== 0 && sound.seek !== sound.duration() && !isSlide) {
        const percent = (sound.seek() * 100) / sound.duration();

        setPercent(percent);
      }
    }, 1000);

    return () => {
      clearInterval(interval);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSlide]);

  const onChangeSlider = (e) => {
    const { value } = e.target;

    setSlide(true);

    setPercent(value);
  };

  const setSeek = () => {
    const currentSeek = (percent * sound.duration()) / 100;

    sound.seek(currentSeek);
    setSlide(false);
  };

  const onClickPlay = () => {
    setPlaying(true);
    sound.play();
  };

  const onClickPause = () => {
    setPlaying(false);
    sound.pause();
  };

  return (
    <div className="flex items-center">
      <div
        className={`${styles["player-container"]} shadow-custom py-3 px-4 rounded-full flex items-center justify-center`}
      >
        <div className="text-xl text-cyan-700 cursor-pointer">
          {isPlaying ? (
            <div className="animate-spin" onClick={onClickPause}>
              <i className="icon-portfolio-pause"></i>
            </div>
          ) : (
            <i className="icon-portfolio-play3" onClick={onClickPlay}></i>
          )}
        </div>
        <div
          className={`${styles["player-control"]} flex items-center relative`}
        >
          <label htmlFor="player-slider" className="w-0 overflow-hidden h-0">
            Player slide
          </label>
          <input
            type="range"
            name="player-slider"
            id="player-slider"
            value={percent}
            onChange={onChangeSlider}
            onMouseUp={setSeek}
            className={styles["player-slide"]}
          />
          <div
            className="absolute bg-cyan-700 h-1"
            style={{ width: `${percent}%` }}
          ></div>
        </div>
      </div>
      <Lottie
        options={defaultOptions}
        height={40}
        width={40}
        isPaused={!isPlaying}
      />
    </div>
  );
};

AudioPlayer.propTypes = {};

export default AudioPlayer;
