// Libraries
import React from "react";
import Image from "next/image";

// Components
import HeaderMenu from "components/HeaderMenu";

const defaultMenu = [
  { value: "home", label: "Home" },
  { value: "about", label: "About" },
  { value: "skills", label: "Skills" },
  { value: "experiences", label: "Experiences" },
  { value: "portfolio", label: "Portfolio" },
  { value: "blog", label: "Blog" },
  { value: "contact", label: "Contact" },
];

const Header = () => {
  return (
    <div
      className="fixed top-0 w-full bg-white animate__animated animate__fadeInDown"
      style={{ zIndex: 999999 }}
    >
      <div className="container mx-auto py-5 flex items-center justify-between">
        <a href="#home">
          <Image
            src="/images/nltruongvi-logo.png"
            width={140}
            height={41}
            alt="Vinlt portfolio"
          />
        </a>
        <div
          id="menu"
          className="lg:flex relative hidden text-lg font-medium items-center gap-5"
        >
          {defaultMenu.length
            ? defaultMenu.map((item) => {
                return (
                  <div
                    data-menuanchor={item.value}
                    className="menu-item cursor-pointer border-b border-transparent text-gray-800 transition-all hover:text-cyan-700 duration-300"
                    key={item.value}
                  >
                    <a href={`#${item.value}`}>{item.label}</a>
                  </div>
                );
              })
            : null}
          <div
            id="line-bottom"
            className="absolute bottom-0 w-5 bg-cyan-700 transition-all duration-700"
          ></div>
        </div>
        <div className="lg:hidden block">
          <HeaderMenu menu={defaultMenu} />
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
