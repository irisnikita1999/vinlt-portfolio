// Libraries
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import TypeIt from "typeit";

// Components
import Header from "./Header";

// Hooks
import useLoading from "hooks/useLoading";

// Utils
import { addClassBySelector } from "utils";

const Layout = ({ meta, children }) => {
  const { title, description, keywords, ogs } = meta;

  const loading = useLoading(
    <div
      id="loading-content"
      className="text-2xl font-semibold w-full text-center"
    ></div>
  );

  useEffect(() => {
    loading.show();

    setTimeout(() => {
      new TypeIt("#loading-content", {
        speed: 30,
        waitUntilVisible: true,
      })
        .type("Hi, My name is Vi", { delay: 300 })
        .delete()
        .type("Welcome to my portfolio!", { delay: 300 })
        .delete()
        .type("Let go!", { delay: 300 })
        .move(-4, { delay: 300 })
        .type("'s", { delay: 300 })
        .exec(() => {
          loading.hide();

          addClassBySelector(".intro-child", "animate__fadeInUp");
          addClassBySelector("#intro-avatar", "animate__fadeInRight");
          addClassBySelector("#layout", "opacity-100");
        })
        .go();
    }, 100);
  }, []);

  return (
    <div id="layout" className="opacity-0">
      <Head>
        <title>{title}</title>
        <meta content="text/html;charset=UTF-8" />
        <meta name="keywords" content={keywords.join(", ")} />
        <meta name="description" content={description} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta property="og:title" content={ogs.title || title} />
        <meta
          property="og:description"
          content={ogs.description || description}
        />
        <meta property="og:image" content={ogs.image} />
        <meta property="og:url" content={ogs.url} />
      </Head>
      <Header />
      {children}
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
  meta: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    author: PropTypes.string,
    ogs: PropTypes.shape({
      image: PropTypes.string,
      url: PropTypes.string,
      title: PropTypes.title,
      description: PropTypes.string,
    }),
  }),
};

Layout.defaultProps = {
  meta: {
    title: "Vinlt - Portfolio",
    description:
      "Hi, My name is Vi. I'm a front-end developer with 2 year experiences. Nice to meet you and hopeful we will be partner",
    keywords: ["Portfolio", "Cv", "Front-end", "Developer", "Job", "Vinlt"],
    ogs: {
      image: "",
      url: "",
      title: null,
      description: null,
    },
  },
};

export default Layout;
