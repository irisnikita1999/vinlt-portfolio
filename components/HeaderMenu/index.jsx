// Libraries
import React, { useState } from "react";

// Styles
import styles from "./styles.module.scss";

const MenuIcon = ({ menu = [] }) => {
  const [isOpen, setOpen] = useState(false);

  const onClickMenuIcon = () => {
    setOpen(!isOpen);
  };

  return (
    <>
      <div
        className={`${styles["icon-wrapper"]} ${
          isOpen ? styles["active"] : ""
        }`}
        onClick={onClickMenuIcon}
      >
        <div></div>
        <div></div>
        <div></div>
      </div>
      <div
        className={`${styles["header-menu"]}`}
        style={{
          zIndex: 198,
          transform: `${isOpen ? "translateX(0)" : "translateX(100vw)"}`,
        }}
      >
        <div className="container mx-auto flex items-center justify-end text-white text-3xl">
          <div className="flex flex-col gap-10 h-screen justify-center text-right">
            {menu.length
              ? menu.map((item) => (
                  <div
                    className={`${styles["menu-item"]} header-menu-item`}
                    data-header-menu-anchor={item.value}
                    key={item.value}
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    <a href={`#${item.value}`}>{item.label}</a>
                  </div>
                ))
              : null}
          </div>
        </div>
      </div>
    </>
  );
};

MenuIcon.propTypes = {};

export default MenuIcon;
