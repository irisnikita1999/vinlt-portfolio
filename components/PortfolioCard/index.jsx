// Libraries
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";

// Styles
import styles from "./styles.module.scss";
import { isMobile } from "utils";

PortfolioCard.defaultProps = {
  skills: ["Html", "Css", "Javascript", "Webflow"],
  images: [
    {
      description: "Home page",
      src: "/images/projects/moneytech/home-page.png",
    },
    {
      description: "About us page",
      src: "/images/projects/moneytech/about-us-page.png",
    },
    {
      description: "Contact us page",
      src: "/images/projects/moneytech/contact-us-page.png",
    },
    {
      description: "Finance page",
      src: "/images/projects/moneytech/finance-page.png",
    },
    {
      description: "News page",
      src: "/images/projects/moneytech/news-page.png",
    },
    {
      description: "Pinnacle partners page",
      src: "/images/projects/moneytech/pinnacle-partners-page.png",
    },
  ],
  description: (
    <>
      We are Australia&apos;s only non-bank financial institution that offers
      finance, payments and FX capabilities. <br /> This gives us a unique
      position to help SMEs who are the growth engine of Australia and employ
      the majority of Australians. <br /> Our working capital solutions, market
      leading FX and an enterprise level payments platform gives you control
      over YOUR cashflow and payments, saving you time and money so you can
      concentrate on doing what you love!
    </>
  ),
  link: null,
};

function PortfolioCard({
  children,
  className,
  skills,
  images,
  title,
  description,
  link,
}) {
  const childRef = useRef(null);

  const [isOpen, setOpen] = useState(false);

  const closeModal = () => {
    setOpen(false);
  };

  useEffect(() => {
    window.addEventListener("click", closeModal);

    return () => {
      window.removeEventListener("click", closeModal);
    };
  }, []);

  const onClickPortfolioCard = (e) => {
    if (isMobile.any()) {
      link && window.open(link);

      return;
    }

    e.stopPropagation();

    setOpen(!isOpen);
  };

  return (
    <div onClick={onClickPortfolioCard}>
      <div
        ref={childRef}
        className={`transition-all ${isOpen ? "opacity-0" : "opacity-100"}`}
      >
        {children}
      </div>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={`${styles["cus-modal"]} ${
          isOpen ? "opacity-100" : "opacity-0"
        } ${className} z-50 w-full text-left h-full absolute duration-500 transition-all top-0 bg-white rounded-lg shadow-custom`}
        style={{
          transform: isOpen ? "scale(1)" : "scale(0)",
        }}
      >
        <div className="flex justify-center">
          <i
            className="icon-portfolio-cancel-circle absolute text-2xl -top-4 bg-white rounded-full transition-all cursor-pointer hover:text-cyan-700"
            onClick={closeModal}
          ></i>
        </div>
        {title ? (
          <div className="grid grid-flow-col grid-cols-5 p-5 gap-5">
            <div className="col-span-2">
              <div className="text-2xl font-bold">{title}</div>
              <p className="mt-5">{description}</p>
              <div className="flex mt-5 gap-4 flex-wrap">
                {skills && skills.length
                  ? skills.map((skill) => (
                      <div
                        key={skill}
                        className="py-1 px-3 border border-cyan-700 rounded-sm"
                      >
                        {skill}
                      </div>
                    ))
                  : null}
              </div>
              <a href={link} rel="noreferrer" target="_blank">
                <button
                  className={`mt-5 ani__delay-1-5 outline-none bg-cyan-700 hover:bg-cyan-600 hover:shadow-custom transition-all rounded-full py-2 px-7 text-white font-medium`}
                >
                  Live demo
                </button>
              </a>
            </div>
            <div
              className="col-span-3 h-106 overflow-auto"
              onScroll={(e) => {
                e.stopPropagation();
              }}
            >
              {images && images.length
                ? images.map((image, index) => (
                    <div key={index}>
                      <div className="relative h-96 overflow-hidden rounded-md">
                        <Image
                          objectFit="cover"
                          objectPosition="top"
                          layout="fill"
                          alt={image.description}
                          src={image.src}
                        />
                      </div>
                      <div className="whitespace-nowrap text-center italic py-2">
                        {image.description}
                      </div>
                    </div>
                  ))
                : null}
            </div>
          </div>
        ) : (
          <div className="flex items-center justify-center h-full">
            <strong>Sorry, This content not yet updated</strong>
          </div>
        )}
      </div>
    </div>
  );
}

export default PortfolioCard;
