import React from "react";
import ReactDOM from "react-dom";

const LoadingComponent = ({ component }) => (
  <div
    style={{ zIndex: 99999999 }}
    className="fixed top-0 flex justify-center items-center h-screen w-screen bg-white"
  >
    {component ? component : "Loading"}
  </div>
);

const useLoading = (component) => {
  const show = () => {
    const loadingEl = document.createElement("div");
    loadingEl.setAttribute("id", "loading-container");
    loadingEl.setAttribute("class", "animate__animated");

    document.body.appendChild(loadingEl);

    ReactDOM.render(<LoadingComponent component={component} />, loadingEl);
  };

  const hide = () => {
    if (document.getElementById("loading-container")) {
      document
        .getElementById("loading-container")
        .classList.add("animate__fadeOut");

      setTimeout(() => {
        document.getElementById("loading-container").remove();
      }, 1000);
    }
  };

  return { show, hide };
};

useLoading.propTypes = {};

export default useLoading;
